# quiver.vim
Quiver is a simple vim plugin for toggling the arrow keys on and off. Some people prefer to keep the arrow keys off by default to either help them learn the h-j-k-l movement keys or just to prevent accidentally pressing them. However, for other people using your machine, this can be a pain. With this plugin, you can easily turn on and off your arrow keys in vim and allow others to once again use your machine.

## Installation

1. For users of [pathogen.vim](https://github.com/tpope/vim-pathogen), you can clone the repository directly into your `bundle` directory.

   ```bash
   $ git clone https://github.com/hplewis/vim-quiver ~/.vim/bundle/vim-quiver
   ```

## Usage

Two common uses for this plugin are:

1. Use the `ToggleArrowKeys` function directly in vim.

   From normal mode:
   ```
   :ToggleArrowKeys<CR>
   ```

2. Create a mapping for the `ToggleArrowKeys` command.

   For example:
   ```
   nnoremap <Leader>k :ToggleArrowKeys<CR>
   ```

I personally suggest the second option if you are going to be toggling a lot. If not, then the first option is fine.

## Settings

Quiver only has one setting option, the initial state of the arrow keys. You can set whether or not to enable or disable the arrow keys on vim startup. This does not prevent toggling.

To turn on arrow keys on vim startup, put the following line in your `~/.vimrc` file.

```
let g:arrow_keys_enabled = 1
```

By default this is set to `0` (disable arrow keys on startup).

## License

MIT
