


function! quiver#enableArrowKeys()
    unmap <Up>
    unmap <Down>
    unmap <Left>
    unmap <Right>
    iunmap <Up>
    iunmap <Down>
    iunmap <Left>
    iunmap <Right>
endfunction

function! quiver#disableArrowKeys()
    map <Up> <Nop>
    map <Down> <Nop>
    map <Left> <Nop>
    map <Right> <Nop>
    imap <Up> <Nop>
    imap <Down> <Nop>
    imap <Left> <Nop>
    imap <Right> <Nop>
endfunction

function! quiver#toggleArrowKeys()
    if g:arrow_keys_enabled
        call quiver#disableArrowKeys()
        let g:arrow_keys_enabled = 0
    else
        call quiver#enableArrowKeys()
        let g:arrow_keys_enabled = 1
    endif
endfunction

function! quiver#setInitialMode()
    if g:arrow_keys_enabled
        call quiver#enableArrowKeys()
    else
        call quiver#disableArrowKeys()
    endif
endfunction
