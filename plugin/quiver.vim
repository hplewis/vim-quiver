

" Default mode is off
if !exists("g:arrow_keys_enabled")
    let g:arrow_keys_enabled = 0
endif

call quiver#setInitialMode()

" Alias ToggleArrowKeys to call the internal quiver function
command! ToggleArrowKeys call quiver#toggleArrowKeys()
